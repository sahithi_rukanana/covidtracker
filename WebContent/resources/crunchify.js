jQuery(document).ready(function($) {
	
	var gs1 = "Germany";
	apicall(gs1);
	selection = gs1;
	$('#crunchifyMessage').html("<h4>Source API: https://api.covid19api.com/summary </h4>")
	
});

var globalInfo = "";
var allStats = "";
var allRates = " ";
var confirmedCases = "";
var selection = "";
function apicall(s1){

$.ajax({
	  url: 'https://api.covid19api.com/summary',
	  dataType: 'json',
	  success: function(data) {
		
			  for ( var i = 0; i < data.Countries.length; i++) {
					var obj = data.Countries[i];
					if(obj.Country==s1){
					var totCon= obj.TotalConfirmed;
					var totDeth= obj.TotalDeaths;
					var totRec = obj.TotalRecovered;
					var date = obj.Date;
					
					var dateStr = date.toString();
					var posdate = dateStr.indexOf("T");
					var postime = dateStr.indexOf("Z");
					var datePart = dateStr.substring(0, posdate);
					var timePart = dateStr.substring(posdate+1, postime);
					
					confirmedCases = totCon.toString();
					
					document.getElementById("totCon").innerHTML=totCon;
					document.getElementById("totDet").innerHTML=totDeth;
					document.getElementById("totRec").innerHTML=totRec;
					document.getElementById("updatedTime").innerHTML="Last Update :   "+ datePart + " ,  " + timePart +"   GMT";
			  		}
			  //}
		  }
	    
	  },
	  error: function(){
          //console.log(allStats);
          //console.log(s1);
		  $('#crunchifyMessage').html("<h4>Source: COVID Database </h4>")
          for (var i = 0; i < allStats.length; i++) {
      			   if(allStats[i].CountryName == s1){
      				   //alert(creditC[i].Country_Id);
      				var totCon= allStats[i].TotalConfirmedCases;
					var totDeth= allStats[i].TotalDeaths;
					var totRec = allStats[i].TotalRecovered;
					var date = allStats[i].Date;
					//alert(JSON.stringify(obj.Date));
					var dateStr = date.toString();
					var posdate = dateStr.indexOf("T");
					var postime = dateStr.indexOf("Z");
					var datePart = dateStr.substring(0, posdate);
					var timePart = dateStr.substring(posdate+1, postime);

					document.getElementById("totCon").innerHTML=totCon;
					document.getElementById("totDet").innerHTML=totDeth;
					document.getElementById("totRec").innerHTML=totRec;
					document.getElementById("updatedTime").innerHTML="Last Update :   "+ datePart + " ,  " + timePart +"   GMT";      			   
      		  }
      		}
          
        	  }
	});
	//alert(confirmedCases);
	
}


function GetSelectedValue(){
	var e = document.getElementById("country");
	var result_val = e.options[e.selectedIndex].value;
	var result = e.options[e.selectedIndex].text;
	//console.log(JSON.stringify(result));
	//gs1 = result;
	apicall(result);
	showStat(result); 
	displayTable(globalInfo);
}

function GetSelectedText(){
	var e = document.getElementById("country");
	var result = e.options[e.selectedIndex].text;
}


function mediTable(hosps) {
	
	var hospitals = hosps;
	//globalInfo = hosps;
	
	console.log("In mediTable method");
    var col = [];
    for (var i = 0; i < hospitals.length; i++) {
        for (var key in hospitals[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    //alert(col);
    // Create a table.
    var table = document.createElement("table");

    // Create table header row using the extracted headers above.
    var tr = table.insertRow(-1);                   // table row.

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      // table header.
        th.innerHTML = col[i];
        tr.appendChild(th);
    }

    // add json data to the table as rows.
    for (var i = 0; i < hospitals.length; i++) {

        tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = hospitals[i][col[j]];
        }
    }

    // Now, add the newly created table with json data, to a container.
    var divShowData = document.getElementById('hospitalData');
    divShowData.innerHTML = "";
    divShowData.appendChild(table);
    
    //document.getElementById('msg').innerHTML = '<br />You can later <a href="https://www.encodedna.com/javascript/dynamically-add-remove-rows-to-html-table-using-javascript-and-save-data.htm" target="_blank" style="color:#1464f4;text-decoration:none;">get all the data from table and save it in a database.</a>';
}

function displayTable(creditC){
	globalInfo = creditC;
	var e = document.getElementById("country");
	var result_val = e.options[e.selectedIndex].value;
	//alert(result_val);
	var tabledatanew = [];
	
	
	for (var i = 0; i < creditC.length; i++) {
		//console.log(creditC[i]);
		var rows = creditC[i];
		 for (j in rows) {
		   if(j == "Country_Id"){
			   if(creditC[i].Country_Id == result_val){
				   //alert(creditC[i].Country_Id);
				   tabledatanew.push(creditC[i]);
			   }
		   }
		  }
		}
	
	
const table = new Tabulator("#example-table", {
  height: "auto",
  width: "auto",
  data: tabledatanew,
  layout:"fitData",
  layout: "fitColumns",
  autoResize:true,
  columns: [{
    title: "Hospital_Name",
    field: "Hospital_Name"
    
  }, {
	title: "Location",
	field: "Location"
	
  }, {
	title: "Available Beds",
	field: "Bed_Count",
    width: 180
  }, {
    title: "Available Ventrilators",
    field: "Ventrilator_Count",
    width: 180
  }, ],
});

}

function displayStats(statResult){
	//alert("LLL");
	allStats = statResult;
	
}

function setRates(rateResult){
	
	allRates = rateResult;
	//alert(selection);
	showStat(selection);
}

function showStat(gs1){
	
	//console.log(allRates);
	for (var i = 0; i < allRates.length; i++) {
		
		   if(allRates[i].Country_Name == gs1){
			var infRate= allRates[i].Infection_Rate;
			
			if( infRate >= 1){
				$('#statusMessage').html("<p style= font-size:20px;background-color:red;color:white>The selected country is in DANGER ZONE. </p>");
			}else{
				$('#statusMessage').html("<p style= font-size:20px;background-color:green;color:white>The selected country is in SAFE ZONE.</p>");
			}
			      			   
	 }
	}
}
