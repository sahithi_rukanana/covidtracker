<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<!-- let's add tag srping:url -->
<spring:url value="/resources/crunchify.css" var="crunchifyCSS" />
<spring:url value="/resources/crunchify.js" var="crunchifyJS" />
<spring:url value="/resources/tabulator.css" var="tabCSS" />
<spring:url value="/resources/tabulator.js" var="tabJS" />
<spring:url value="/resources/tabulator.min.css" var="tabminCSS" />
<spring:url value="/resources/tabulator.min.js" var="tabminJS" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="${crunchifyCSS}" rel="stylesheet" />
<script src="${crunchifyJS}"></script>
<link href="${tabCSS}" rel="stylesheet" />
<script src="${tabJS}"></script>
<link href="${tabminCSS}" rel="stylesheet" />
<script src="${tabminJS}"></script>
<!-- Finish adding tags -->

<title>COVID-19 Tracker</title>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var creditC = ${credit};
	var statResult = ${result1};
	var rateResult = ${result2};
	//mediTable(creditC);
	//$("#example-table").tabulator();
	displayTable(creditC);
	
	displayStats(statResult);
	
	setRates(rateResult);
		//console.log(table.getData());
});

</script>
<style type="text/css">
body {
	background-color: white;
}
</style>
</head>
<body>${message}
	<h2 align="center">COVID-19 Tracker</h2>
	<div align="center">
	<form>
	  Country Name &nbsp;
	  <select id="country" name="country">
	    <option value="1" selected="selected" >Germany</option>
	    <option value="2">India</option>
	    <option value="3">United States of America</option>
	    <option value="4">United kingdom</option>
	    <option value="5">China</option>
	    <option value="6">Japan</option>
	    <option value="7">Bangladesh</option>
	    <option value="8">Australia</option>
	    <option value="9">Netherlands</option>
	  </select>
	  <br>
	  <br>
	  <input value="Submit" type="button" onclick="GetSelectedValue()">
	</form>
	<br>
	 <br>
	<h2>COVID-19 Statistics Table</h2>
<table id="result" style="width:60%">
  <tr>
    <th>Total Confirmed</th>
    <th>Total Deaths</th>
    <th>Total Recovered</th>
  </tr>
  <tr>
    <td id="totCon">0</td>
    <td id="totDet">0</td>
    <td id="totRec">0</td>
  </tr>
</table>
<div align="center"  id="statusMessage"></div>
</div>
<br><br>
<div align="center">
	<h2>Medical Information</h2>
	<!-- <input type='button' onclick='mediTable(creditC)' 
    	value='Medical Information' /> -->
   
    <!-- <label for="comment2"></label> -->
    <div id="example-table"></div>
 
    <p id='showData'></p>
    <p id='hospitalData'></p>
    
    <!-- medicalInfo() <p id='msg'></p> -->
</div>	 
	<br>
	<br>

	<!--
	<div
		style="font-family: verdana; padding: 10px; border-radius: 10px; font-size: 12px; text-align: center;">
 		
 		 <h2>Checkout this font color. Loaded from 'crunchify.css' file under '/WebContent/go/' folder</h2> -->
 		<div align="center"  id="crunchifyMessage"></div>
 		<div align="center"><h4 id="updatedTime"></h4></div>
		
</body>
</html>